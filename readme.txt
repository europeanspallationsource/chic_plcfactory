#Create local ccdb file

#run plc factory
python plcfactory.py --ccdb=chopper0301.ccdb.zip --plc-beckhoff --e3 labs-embla_chop-chic-03 -d LabS-Embla:Chop-CHIC-03

#add line to CONFIG_MODULE
CALC_DEP_VERSION:=3.7.1

#run make
make build
make install

