""" PLC Factory: Global variables """

__author__     = "Gregor Ulm"
__copyright__  = "Copyright 2016, European Spallation Source, Lund"
__license__    = "GPLv3"

# timestamp for names of output files
timestamp              = None

# the CCDB backend
ccdb                   = None

# the root installation slot
root_installation_slot = None

# the name of the EEE module
eee_modulename         = None
# the name of the E3 module
e3_modulename          = None

# the name of the EEE snippet
eee_snippet            = None
# the name of the E3 snippet
e3_snippet             = None
