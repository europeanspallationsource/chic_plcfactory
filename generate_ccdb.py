from ccdb_factory import CCDB_Factory
 
factory = CCDB_Factory()
 
# Add the PLC device with name
plc = factory.addBECKHOFF("LabS-Embla:Chop-CHIC-04")
 
# link to definition file, local or in repo, to PLC device
plc.addLink("EPI[CHOP_CHIC.def]", "https://bitbucket.org/europeanspallationsource/chop_chic")
plc.setProperty("EPI VERSION", "master")
 
# two devices controlled by the PLC
drive_names = ["LabS-Embla:Chop-Drv-0401"]
 
# add sub devices to PLC if needed
plc.setControls(drive_names)
 
 
#Go through all the devices in cmses
for d in drive_names:
    drive = factory.addDevice("DRV_TYPE", d)
    drive.addLink("EPI[CHOP_DRV.def]", "https://bitbucket.org/europeanspallationsource/chop_drv")
    drive.setProperty("EPI VERSION", "master")
 
# Dump our CCDB
factory.dump("local_file")






#from ccdb_factory import CCDB_Factory
 
#factory = CCDB_Factory()
 
# Add the PLC device with name
#plc = factory.addBECKHOFF("LabS-Embla:Chop-CHIC-04")
 
# link to definition file, local or in repo, to PLC device
#factory.addLink("chic_type", "EPI[CHOP_CHIC.def]", "https://bitbucket.org/europeanspallationsource/chop_chic")
 
# two devices controlled by the PLC
#drives = ["LabS-Embla:Chop-Drv-0401"]
 
# add sub devices to PLC if needed
#plc.setControls(drives)
 
#Go through all the devices in cmses
#for drv in drives:
#    factory.addDevice("chic_type", drv)
      
 
#factory.device("LabS-Embla:Chop-Drv-0401").addLink("EPI[CHOP_DRV.def]", "https://bitbucket.org/europeanspallationsource/chop_drv")
 
# Dump our CCDB
