from ccdb_factory import CCDB_Factory
 
factory = CCDB_Factory()


#devices controlled by PLC
#plc_device_name = "LabS-Embla:Chop-CHIC-04"
# device_names = ["LabS-Embla:Chop-Drv-0401", "LabS-Embla:Chop-Drv-0402"]
plc_device_name = "LabS-Embla:Chop-CHIC-04"
device_names = ["LabS-Embla:Chop-Drv-0401"]

# Add the PLC device with name
plc = factory.addBECKHOFF(plc_device_name)


# link to definition file, local or in repo, to PLC device
plc.addLink("EPI[CHOP_CHIC.def]", "https://bitbucket.org/europeanspallationsource/chop_chic")
plc.setProperty("EPI VERSION", "master")
 
# add sub devices to PLC if needed
plc.setControls(device_names)
 
#Go through all the devices in the array 
for d in device_names:
    drive = factory.addDevice("DRV_TYPE", d)
    drive.addLink("EPI[CHOP_DRV.def]", "https://bitbucket.org/europeanspallationsource/chop_drv")
    drive.setProperty("EPI VERSION", "master")
 
# Dump our CCDB
factory.dump("local_test_file")
