
from ccdb_factory import CCDB_Factory

factory = CCDB_Factory()

# link to definition file, local or in repo, to sub device
#factory.addLink("detector_utils", "EPI[beckhoff]", "https://bitbucket.org/europeanspallationsource/cms-string", "/home/nicklasholmberg2/git/ics-dev-nh/ics_plc_factory/beckhoff.def")

# Name to specify as argument in plcfactory.py
plc = factory.addBECKHOFF("LabS-Embla:Chop-CHIC-03")

# link to definition file, local or in repo, to PLC device
plc.addLink("EPI[beckhoff]", "https://bitbucket.org/europeanspallationsource/cms-string", "/epics/iocs/e3-7.0.3/ics_plc_factory/beckhoff.def")

# two devices controlled by the PLC, if using factory.addLink
cmses = [ "LabS-Embla:Chop-Drv-0301"]

# add sub devices to PLC if needed
#plc.setControls(cmses)

#Go through all the devices in cmses
for cms in cmses:
    factory.addDevice("chopper_chic", cms)

factory.dump("beckhoff")



