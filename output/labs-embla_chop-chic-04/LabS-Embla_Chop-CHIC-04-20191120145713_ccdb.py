#!/usr/bin/env python2

import os
import sys

sys.path.append(os.path.curdir)
sys.path.append(os.path.abspath(os.path.dirname(__file__)))

from ccdb_factory import CCDB_Factory

factory = CCDB_Factory()


#
# Adding Beckhoff PLC: LabS-Embla:Chop-CHIC-04
#
plc = factory.addBECKHOFF("LabS-Embla:Chop-CHIC-04")
# Properties
plc.setProperty("EPICSModule", "[]")
plc.setProperty("EPICSSnippet", "[]")
plc.setProperty("PLCF#EPICSToPLCDataBlockStartOffset", "12288")
plc.setProperty("PLCF#PLCToEPICSDataBlockStartOffset", "0")
plc.setProperty("PLCF#PLC-EPICS-COMMS: MBPort", "502")
plc.setProperty("PLCF#PLC-EPICS-COMMS: MBConnectionID", "255")
plc.setProperty("PLCF#PLC-EPICS-COMMS: S7ConnectionID", "256")
plc.setProperty("PLCF#PLC-EPICS-COMMS: S7Port", "2000")
plc.setProperty("PLCF#PLC-EPICS-COMMS:Endianness", "LittleEndian")
plc.setProperty("PLCF#PLC-EPICS-COMMS: InterfaceID", "16#40")
plc.setProperty("PLCF#PLC-DIAG:Max-IO-Devices", "1")
plc.setProperty("PLCF#PLC-DIAG:Max-Local-Modules", "10")
plc.setProperty("PLCF#PLC-DIAG:Max-Modules-In-IO-Device", "10")
plc.setProperty("EPI VERSION", "master")
# External links
plc.addLink("EPI[CHOP_CHIC.def]", "https://bitbucket.org/europeanspallationsource/chop_chic")

#
# Adding device drv1 of type DRV_TYPE
#
dev = plc.addDevice("DRV_TYPE", "drv1")
# Properties
dev.setProperty("EPI VERSION", "master")
# External links
dev.addLink("EPI[CHOP_DRV.def]", "https://bitbucket.org/europeanspallationsource/chop_drv")


#
# Saving the created CCDB
#
factory.save("LabS-Embla:Chop-CHIC-04")
