#!/usr/bin/env python2

import os
import sys

sys.path.append(os.path.curdir)
sys.path.append(os.path.abspath(os.path.dirname(__file__)))

from ccdb_factory import CCDB_Factory

factory = CCDB_Factory()


#
# Adding Beckhoff PLC: LabS-Embla:Chop-CHIC-03
#
plc = factory.addBECKHOFF("LabS-Embla:Chop-CHIC-03")
# Properties
plc.setProperty("EPICSModule", "[]")
plc.setProperty("EPICSSnippet", "[]")
plc.setProperty("PLCF#EPICSToPLCDataBlockStartOffset", "12288")
plc.setProperty("PLCF#PLCToEPICSDataBlockStartOffset", "0")
plc.setProperty("PLCF#PLC-EPICS-COMMS: MBPort", "502")
plc.setProperty("PLCF#PLC-EPICS-COMMS: MBConnectionID", "255")
plc.setProperty("PLCF#PLC-EPICS-COMMS: S7ConnectionID", "256")
plc.setProperty("PLCF#PLC-EPICS-COMMS: S7Port", "2000")
plc.setProperty("PLCF#PLC-EPICS-COMMS:Endianness", "LittleEndian")
plc.setProperty("PLCF#PLC-EPICS-COMMS: InterfaceID", "16#40")
plc.setProperty("PLCF#PLC-DIAG:Max-IO-Devices", "1")
plc.setProperty("PLCF#PLC-DIAG:Max-Local-Modules", "10")
plc.setProperty("PLCF#PLC-DIAG:Max-Modules-In-IO-Device", "10")
# External links
plc.addLink("EPI[beckhoff]", "https://bitbucket.org/europeanspallationsource/cms-string")



#
# Saving the created CCDB
#
factory.save("LabS-Embla:Chop-CHIC-03")
