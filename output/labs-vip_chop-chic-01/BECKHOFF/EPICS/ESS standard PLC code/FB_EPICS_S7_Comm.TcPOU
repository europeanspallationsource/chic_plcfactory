<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4022.18">
  <POU Name="FB_EPICS_S7_Comm" Id="{5bb54db1-6fe3-4b17-2b0f-000000000009}" SpecialFunc="None">
    <Declaration><![CDATA[
FUNCTION_BLOCK FB_EPICS_S7_Comm
VAR_INPUT
	nS7Port			:UDINT	:= 2000;			//Server port. Leave empty for default 2000
	nPLC_Hash		:DINT;					//Hash written during XML generation at PLC factory
	tSendTrig		:TIME	:= T#200MS;			//Frequency of pushed data
	
END_VAR
VAR_OUTPUT
	nCase			:INT	:= 0;				//Status. 0=Init, 1=Close conn., 2=Listen, 3=Accept, 4=Send data
	bConnected		:BOOL;					//TCP/IP connection accepted
	bError			:BOOL;					//Error in connection
END_VAR
VAR
	sSrvNetID		:T_AmsNetId	:='';			// Local ID if empty
	sLocalHost		:STRING(15)	:='';			// Local if empty
	bConnectEpics		:BOOL		:= TRUE;		//Open or closes TCP/IP connection

	fb_SocketCloseAll	:FB_SocketCloseAll;
	bCloseAll		:BOOL;
	bCloseAllBusy		:BOOL;
	bCloseAllError		:BOOL;
	nCloseAllErrID		:UDINT;

	hServer			:T_HSERVER;
	bListen			:BOOL;

	fbServerConnection	:FB_ServerClientConnection;
	bSvrConnect		:BOOL;
	bSvrBusy		:BOOL;
	bSvrError		:BOOL;
	nSvrErrID		:UDINT;
	hSocket			:T_HSOCKET;
	eSvrState		:E_SocketConnectionState;

	fb_SocketSend		:FB_SocketSend;
	bSend			:BOOL;
	bSendBusy		:BOOL;
	bSendError		:BOOL;
	nSendErrID		:UDINT;

	f_bConnectEpics		:F_TRIG;
	t_Init			:TON;
	//tInitTime		:TIME:= T#3S;
	t_Push			:TON;
	nNumberOfErr		:INT:=0;

	//blink & counter functions
	fb_Pulse		:FB_Pulse;		//Pulse/counter function block
	nCount			:UINT;

	i				:INT:=3;		//index for filling data array loop
	nPLC_HashH		:UINT;			//Least significant part of the hash
	nPLC_HashL		:UINT;			//Most significant part of the hash
END_VAR

]]>
    </Declaration>
    <Implementation>
      <ST><![CDATA[(*
**********************EPICS<-->Beckhoff integration at ESS in Lund, Sweden*******************************
TCP/IP server on Bechoff for EPICS<--Beckhoff communication flow.
Modbus Server on Beckhoff for EPICS-->Beckhoff communication flow.
Created by: Andres Quintanilla (andres.quintanilla@esss.se)
            Miklos Boros (miklos.boros@esss.se)
Date: 06/04/2018
Notes: TCP/IP server pushes data to the EPICS IOC connected. Modbus connection is open for R/W.
Code must not be changed manually. Code is generated and handled by PLC factory at ESS.
Functionality: 
Step 0: Reset all flags to start sequence
Step 1: Close all previous connections
Step 2: Initialize TCP/Ip server Listener
Step 3: Accept any incoming connection request. Matching connection is validated via the input nPLC Hash at EPICS level.
Step 4: Sends data to epics constantly using input tSendTrig as frequency
**********************************************************************************************************
*)

CASE nCase OF 
	0:// Reset all flags to start sequence
	bCloseAll		:=FALSE;
	bListen 		:=FALSE;
	bSvrConnect		:=FALSE;
	bSend 			:=FALSE;
	t_Init.IN		:=FALSE;
	t_Push.IN		:=FALSE;
	nNumberOfErr		:=0;
	IF bConnectEpics THEN
		nCase := nCase + 1;
	END_IF

	1:// Close all previous connections
	t_Init.PT	:= T#3S;
	t_Init.IN	:= TRUE;
	bCloseAll	:= TRUE;
	IF NOT bCloseAllBusy AND T_Init.Q AND NOT bSvrBusy THEN
		T_Init.IN	:= FALSE;
		bCloseAll	:= FALSE;
		nCase		:= nCase + 1;
	END_IF

	2:// Initialize TCP/IP server Listener
	bListen	:= TRUE;
	nCase	:= nCase + 1;

	3:// Start server and wait to accept a connection
	t_Init.PT	:= T#600S;
	t_Init.IN	:= TRUE;
	bSvrConnect	:= TRUE;
	IF eSvrState = eSOCKET_CONNECTED AND NOT bSvrBusy THEN
		t_Init.IN		:= FALSE;
		nCase			:= nCase + 1;
	END_IF
	IF t_Init.Q THEN
		t_Init.IN		:= FALSE;
		bListen			:= FALSE;
		bSvrConnect		:= FALSE;
		nCase			:= 0;
	END_IF

	4:// Push data to EPICS
	t_Push.IN	:= TRUE;
	bSend 		:= TRUE;
	IF t_Push.Q THEN
		t_Push.IN	:= FALSE;
		bSend 		:= FALSE;
		IF bSendError THEN
			nNumberOfErr	:= nNumberOfErr + 1;	
		END_IF
	END_IF
	IF nNumberOfErr = 20 THEN
		t_Push.IN	:= FALSE;
		bSend 		:= FALSE;
		bListen		:= FALSE;
		bSvrConnect	:= FALSE;
		nNumberOfErr	:= 0;
		nCase		:= 0;
	END_IF
END_CASE

//Prepares array to be sent
nPLC_HashL := DINT_TO_UINT(SHR(nPLC_Hash,16));
nPLC_HashH := DINT_TO_UINT(nPLC_Hash);
EPICS_GVL.aDataS7[1]:=nPLC_HashL;
EPICS_GVL.aDataS7[0]:=nPLC_HashH;
EPICS_GVL.aDataS7[2]:=nCount;


//TCP IP communication function blocks
F_CreateServerHnd(
	sSrvNetID 	:=sSrvNetID,
	sLocalHost 	:=sLocalHost,
	nLocalPort	:=nS7Port,
	nMode		:=LISTEN_MODE_CLOSEALL,
	bEnable		:=bListen,
	hServer		:=hServer);

fbServerConnection(
	hServer	:= hServer,
	eMode	:= eACCEPT_ALL,
	sRemoteHost:= '',
	nRemotePort:=nS7Port,
	bEnable	:= bSvrConnect,
	tReconnect	:= T#3S,
	bBusy		=>bSvrBusy,
	bError		=>bSvrError,
	nErrID		=>nSvrErrID,
	hSocket		=>hSocket,
	eState		=>eSvrState);

fb_SocketCloseAll(
	sSrvNetId:=sSrvNetID,
	bExecute:=bCloseAll,
	tTimeout:=T#5S,
	bBusy=>bCloseAllBusy,
	bError=>bCloseAllError,
	nErrId=>nCloseAllErrID);

fb_SocketSend(
	sSrvNetId:=sSrvNetID,
	hSocket:=hSocket,
	cbLen:=SIZEOF(EPICS_GVL.aDataS7),
	pSrc:=ADR(EPICS_GVL.aDataS7),
	bExecute:=bSend,
	tTimeout:=T#5S,
	bBusy=>bSendBusy,
	bError=>bSendError,
	nErrId=>nSendErrID);

//Counter function
IF nCase = 4 THEN 
	fb_Pulse.bEn := TRUE;
	ELSE
	fb_Pulse.bEn := FALSE;
END_IF
fb_Pulse(bEn:= , tTimePulse:=T#1S , bPulse=> , nCount=>nCount );


//Triggers
f_bConnectEpics(CLK:=bConnectEpics, Q=>);
IF f_bConnectEpics.Q THEN
	nCase	:= 0;
END_IF

//Timers
t_Init(IN:=, PT:=);
t_Push(IN:=, PT:=tSendTrig);

//Outputs
bConnected	:= eSvrState = eSOCKET_CONNECTED AND nCase = 4 AND NOT bSendError;
bError		:= bCloseAllError OR bSvrError OR bSendError;
]]>
      </ST>
    </Implementation>
  </POU>
</TcPlcObject>
